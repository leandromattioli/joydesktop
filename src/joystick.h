#include <stdio.h>
#include <linux/joystick.h>

#ifndef __JOYSTICK_H__
#define __JOYSTICK_H__

typedef struct {
	int fd;
	char button[12];
	int axis1_x;
	int axis1_y;
	int axis2_x;
	int axis2_y;
	struct js_event rawEvt;
} Joystick;

int joystick_open(Joystick* joyPtr, const char* joystick_device);
int joystick_read_event(Joystick* joyPtr);
void joystick_close(Joystick* joyPtr);
int joystick_get_status(Joystick* joyPtr);

#endif

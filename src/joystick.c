#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "joystick.h"


int joystick_open(Joystick* joy, const char* devName) {
	/* @FIXME: read write for force feedback? */
	memset(joy, 0x00, sizeof(Joystick));
	joy->fd = open(devName, O_RDONLY | O_NONBLOCK);
	if (joy->fd < 0)
		return joy->fd;

	/* maybe ioctls to interrogate features here? */

	return joy->fd;
}


void joystick_close(Joystick* joy) {
	close(joy->fd);
}


int joystick_read_event(Joystick* joy) {
	int bytes;

	bytes = read(joy->fd, &(joy->rawEvt), sizeof(struct js_event));

	if (bytes == -1)
		return 0;

	if (bytes == sizeof(struct js_event))
		return 1;

	printf("Unexpected bytes from joystick:%d\n", bytes);

	return -1;
}


int joystick_get_status(Joystick* joy) {
	int rc;
	struct js_event* evt;

	if (joy->fd < 0)
		return -1;

	// memset(wjse, 0, sizeof(*wjse));
	while ((rc = joystick_read_event(joy) == 1)) {
		evt = &(joy->rawEvt);
		evt->type &= ~JS_EVENT_INIT; /* ignore synthetic events */
		if (evt->type == JS_EVENT_AXIS) {
			switch (evt->number) {
				case 0:
					joy->axis1_x = evt->value;
					break;
				case 1:
					joy->axis1_y = evt->value;
					break;
				case 2:
					joy->axis2_x = evt->value;
					break;
				case 3:
					joy->axis2_y = evt->value;
					break;
			}
		}
		else if (evt->type == JS_EVENT_BUTTON) {
			if (evt->number < 12 && (evt->value == 0 || evt->value == 1))
				joy->button[evt->number] = (char) evt->value;
		}
	}
	return 0;
}

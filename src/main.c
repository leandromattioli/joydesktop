#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <xdo.h>
#include "common.h"
#include "joystick.h"
#include "cfgfile.h"

Joystick joy;
Joystick* joyPtr = &joy;
xdo_t* xdo;
char block[BUTTONS];
ConfigEntry config[BUTTONS];

void close_resources();

// ============================================================================
// Action Handlers
// ============================================================================

void handle_click_oneshot(char joyBtnIdx, char mouseBtnIdx) {
	if(joy.button[joyBtnIdx] == 1 && block[mouseBtnIdx] == 0) {
		block[mouseBtnIdx] = 1; //wait for button release
        xdo_mouse_down(xdo, CURRENTWINDOW, mouseBtnIdx);
	}
	else if(joy.button[joyBtnIdx] == 0 && block[mouseBtnIdx] == 1) {
		block[mouseBtnIdx] = 0;
        xdo_mouse_up(xdo, CURRENTWINDOW, mouseBtnIdx);
	}
}


void handle_click_continuous(char joyBtnIdx, char mouseBtnIdx, long int delay) {
    if(joyPtr->button[joyBtnIdx] == 1) {
        usleep(delay);
        xdo_click_window(xdo, CURRENTWINDOW, mouseBtnIdx);
    }
}


void handle_exit(char joyBtnIdx) {
    if(joyPtr->button[joyBtnIdx] == 1) {
        close_resources();
        exit(0);
    }
}


void handle_command(char joyBtnIdx, const char* command, long int delay) {
    int dummy; //make compiler happy
    if(joyPtr->button[joyBtnIdx] == 1) {
        usleep(delay);
        dummy = system(command);
    }
}

// ============================================================================
// Usage and Verbose Output
// ============================================================================

void usage(const char* argv0) {
    printf("Usage: %s [OPTION]\n", argv0);
    puts("Controls your mouse by a Joystick");
    puts("  -d device    Input device to use (defaults to /dev/input/js0)");
    puts("  -h           Show this help message");
    puts("  -v           Enable verbose mode");
    puts("  -t           Enable test mode (joystick buttons have no actions)");
    puts("  -j           Enable JSON mode (for IPC use)");
    puts("");
    puts("Buttons actions may be overriden by the utility joydesktop-config.");
    puts("Alternatively, one can create or edit the file $HOME/.joydesktoprc");
    puts("Each line in the config file describes a button behaviour.");
    puts("The syntax is:  <button_number>;<action>;<arguments>");
    puts("Default configuration: ");
    config_default();
    config_dump(stdout);
}


void show_joystick_status() {
    int x, y, button;
    x = joyPtr->axis1_x;
    y = joyPtr->axis1_y;
    printf("\rX: %+6d   Y: %+6d    ", x, y);
    for(button = 0; button < BUTTONS; button++)
        printf("B%d: %d   ", button, joyPtr->button[button]);
    fflush(stdout);
}

void show_joystick_status_json() {
    int x, y, button;
    x = joyPtr->axis1_x;
    y = joyPtr->axis1_y;
    printf("{\"x\":%d, \"y\":%d ", x, y);
    for(button = 0; button < BUTTONS; button++)
        printf(",\"b%d\": %d ", button, joyPtr->button[button]);
    printf("}\n");
    fflush(stdout);
}

// ============================================================================
// Main Routine and Close_Resources
// ============================================================================

void close_resources() {
    int i;
    puts("");
    for(i = 0; i < BUTTONS; i++) {
        if(config[i].command != NULL)
            free(config[i].command);
    }
    xdo_free(xdo);
}

int main(int argc, char* argv[]) {
	int rc;
	int x, y;
    int i;
    int c;
    bool verbose = false, test = false, json = false;
    char *device = "/dev/input/js0";

    //Parsing command line
    opterr = 0;
    while((c = getopt(argc, argv, "d:vhtj")) != -1) {
        switch(c) {
        case 'v': 
            verbose = true;
            break;
        case 't':
            test = true;
            break;
        case 'j':
            json = true;
            break;
        case 'd':
            device = optarg;
            break;
        case 'h':
            usage(argv[0]);
            exit(0);
        case '?':
            if(optopt == 'd')
                fprintf(stderr, "Option -%c requires an argument\n", optopt);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option -%c\n", optopt);
            else
                fprintf(stderr, "Unknown character '\\x%x'\n", optopt);
            return 1;
        default:
            abort();
        }
    }

    if(json) {
        puts("[MODE] JSON mode enabled.");
    }
    else if(test) {
        puts("[MODE] Test mode enabled.");
        puts("[WARNING] No desktop action will be performed by the joystick");
    }
    else if(verbose) {
        puts("[MODE] Verbose mode enabled.");
        puts("[HINT] By default, button 3 will exit this daemon.\n");
    }


	rc = joystick_open(joyPtr, device);
	if(rc < 0) {
		perror("[IO_ERROR] Device not found!\n");
		return -1;
	}

	xdo = xdo_new(NULL);
	if(!xdo) {
		perror("[X11_ERROR] Fail to open display!\n");
		return -2;
	}

    //Buttons Configuration
    for(i = 0; i < BUTTONS; i++)
        block[i] = 0;
    if(!config_load())
        config_default();

	while(1) {
		usleep(5000);
		joystick_get_status(joyPtr);

        if(json) {
            show_joystick_status_json();
            usleep(50000);
        }
        else if(verbose || test)
            show_joystick_status();

        if(test || json)
            continue;

		//Mouse movement
		x = joyPtr->axis1_x / 6553;
		y = joyPtr->axis1_y / 6553;
		if(x != 0 || y != 0)
            xdo_move_mouse_relative(xdo, x, y);

        for(i = 0; i < BUTTONS; i++) {
            switch(config[i].action) {
            case CLICK_ONESHOT:
                handle_click_oneshot(i, config[i].mouseButton);
                break;
            case CLICK_CONTINUOUS:
                handle_click_continuous(i, config[i].mouseButton, 50000);
                break;
            case COMMAND:
                handle_command(i, config[i].command, 50000);
                break;
            case EXIT:
                handle_exit(i);
            }
        }
	}
    close_resources();
	return 0;
}

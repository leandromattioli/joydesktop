#define BUTTONS 8

#define string_compare(unsafe,safe) strncmp(unsafe, safe, strlen(safe) + 1)

typedef enum {
    CLICK_ONESHOT,
    CLICK_CONTINUOUS,
    SENDKEY,
    COMMAND,
    EXIT,
    NONE
} Action;


typedef struct {
    Action action;
    char mouseButton;
    char key;
    char* command;
} ConfigEntry; 

#ifndef CFGFILE_H
#define CFGFILE_H

#define CFG_FILE    "/.joydesktoprc"
#define CMD_VOLDOWN "amixer -q sset Master 0.2%-"
#define CMD_VOLUP   "amixer -q sset Master 0.2%+"

void config_default();
const char* config_get_filepath();
bool config_parse_line(char* line);
bool config_load();
void config_dump(FILE* out);

#endif

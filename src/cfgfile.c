#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <linux/limits.h>
#include "common.h"
#include "cfgfile.h"

extern ConfigEntry config[BUTTONS];

void config_default() {
    //Resetting
    int i;
    for(i = 0; i < BUTTONS; i++) {
        config[i].action = NONE;
        config[i].command = NULL;
    }
    //Restoring defaults
    config[0].action = CLICK_ONESHOT;
    config[0].mouseButton = 1;
    config[1].action = CLICK_ONESHOT;
    config[1].mouseButton = 3;
    config[2].action = CLICK_ONESHOT;
    config[2].mouseButton = 2;
    config[3].action = EXIT;
    config[4].action = CLICK_CONTINUOUS;
    config[4].mouseButton = 4;
    config[5].action = CLICK_CONTINUOUS;
    config[5].mouseButton = 5;
    config[6].action = COMMAND;
    config[6].command = (char *) malloc(strlen(CMD_VOLDOWN) + 1);
    strncpy(config[6].command, CMD_VOLDOWN, strlen(CMD_VOLDOWN) + 1);
    config[7].action = COMMAND;
    config[7].command = (char *) malloc(strlen(CMD_VOLUP) + 1);
    strncpy(config[7].command, CMD_VOLUP, strlen(CMD_VOLUP) + 1);
}

bool config_parse_line(char* line) {
    char *part;
    unsigned int joyBtn, mouseBtn;
    //Joystick button
    part = strtok(line, ";");
    if( part == NULL || (sscanf(part, "%u", &joyBtn) != 1) )
        return false;
    //Action type
    if((part = strtok(NULL, ";")) == NULL)
        return false;
    if(string_compare(part, "exit") == 0)
        config[joyBtn].action = EXIT;
    else if(string_compare(part, "none") == 0)
        config[joyBtn].action = NONE;
    else if(string_compare(part, "click_oneshot") == 0) {
        part = strtok(NULL, ";");
        if( (part == NULL || sscanf(part, "%u", &mouseBtn) != 1) )
            return false;
        config[joyBtn].action = CLICK_ONESHOT;
        config[joyBtn].mouseButton = mouseBtn;
    }
    else if(string_compare(part, "click_continuous") == 0) {
        part = strtok(NULL, ";");
        if( (part == NULL || sscanf(part, "%u", &mouseBtn) != 1) )
            return false;
        config[joyBtn].action = CLICK_CONTINUOUS;
        config[joyBtn].mouseButton = mouseBtn;
    }
    else if(string_compare(part, "command") == 0) {
        if( (part = strtok(NULL, ";")) == NULL )
            return false;
        config[joyBtn].action = COMMAND;
        if(config[joyBtn].command != NULL)
            free(config[joyBtn].command);
        config[joyBtn].command = strdup(part);
    }
    else
        return false;
    return true;
}

const char* config_get_filepath() {
    static char filename[PATH_MAX - 1];
    char* home = getenv("HOME");
    if(!home)
        home = ".";
    strncpy(filename, home, strlen(home));
    if(filename[strlen(filename) - 1] == '/') //remove eventual last slash
        filename[strlen(filename) - 1] = '\0';
    strncat(filename, CFG_FILE, strlen(CFG_FILE));
    return filename;
}

bool config_load() {
    const char* filename = config_get_filepath();
    FILE* fp = fopen(filename, "r");
    if(!fp)
        return false;
    fprintf(stdout, "Reading config file %s\n", filename);
    fflush(stdout);
    char line[LINE_MAX];
    unsigned int i = 0;
    while( fgets(line, LINE_MAX, fp) != NULL ) {
        line[strlen(line) - 1] = '\0'; //remove the \0
        if(!config_parse_line(line))
            fprintf(stderr, "[CONFIG] Skipping invalid line %u.\n", i);
        i++;
    }
    fclose(fp);
    return true;
}

void config_dump(FILE* out) {
    int i;
    for(i = 0; i < BUTTONS; i++) {
        fprintf(out, "%u;", i);
        switch(config[i].action) {
        case EXIT:
            fputs("exit", out);
            break;
        case NONE:
            fputs("none", out);
            break;
        case CLICK_ONESHOT:
            fprintf(out, "click_oneshot;%u", config[i].mouseButton);
            break;
        case CLICK_CONTINUOUS:
            fprintf(out, "click_continuous;%u", config[i].mouseButton);
            break;
        case COMMAND:
            fprintf(out, "command;%s", config[i].command);
            break;
        }
        fprintf(out, "\n");
    }
}
